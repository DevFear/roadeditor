﻿using System;
using UnityEngine;
using Cysharp.Threading.Tasks;
using Zenject;
using Core.RoadBuilding;
using Core.RoadBuilding.Designing;
using Core.PhysicsHandlers;

namespace Architecture.Subsystems.RoadEditing
{
    using InputBehaviour;

    public class RoadEditingSubsystem : ISubsystemAsync
    {
        RaycastDetector _detector;
        RoadDesigner _designer;

        IBackgroundSubsystem<MouseData> _mouseInputSubsystem;
        IBackgroundSubsystem<KeyboardInputType> _keyboardInputSubsystem;
        ISubsystemAsync _flightCameraSubsystem;
        ISubsystemAsync<Anchor, RoadDesigner> _movementAnchorSubsystem;

        int layerPlatform;
        RoadSettings settings = new RoadSettings(10f, 2f, 15f);

        bool controlTransferred;

        public RoadEditingSubsystem(RaycastDetector detector, RoadDesigner designer,
            IBackgroundSubsystem<MouseData> mouseInputSubsystem, IBackgroundSubsystem<KeyboardInputType> keyboardInputSubsystem,
            [Inject(Id = "Flight")] ISubsystemAsync flightCameraSubsystem,
            ISubsystemAsync<Anchor, RoadDesigner> movementAnchorSubsystem)
        {
            _detector = detector;
            _designer = designer;

            _mouseInputSubsystem = mouseInputSubsystem;
            _keyboardInputSubsystem = keyboardInputSubsystem;

            _flightCameraSubsystem = flightCameraSubsystem;
            _movementAnchorSubsystem = movementAnchorSubsystem;

            layerPlatform = LayerMask.NameToLayer("Platform"); //заглушка

            controlTransferred = false;
        }

        public async UniTask TransferControl()
        {
            var isControl = true;

            Action<KeyboardInputType> updateKeyboard = type => isControl = UpdateControl(type);

            _mouseInputSubsystem.OnBackgroundEvent += HandleUpdateMouse;
            _keyboardInputSubsystem.OnBackgroundEvent += updateKeyboard;

            while (isControl)
                await UniTask.Yield();

            if (_designer.MaintainedRoad != null)
                _designer.CloseRoad();

            _mouseInputSubsystem.OnBackgroundEvent -= HandleUpdateMouse;
            _keyboardInputSubsystem.OnBackgroundEvent -= updateKeyboard;
        }

        private async void HandleUpdateMouse(MouseData data)
        {
            if (controlTransferred) 
                return;

            if (data.Type == MouseInputType.LeftMouseButtonDown)
            {
                var anchor = _detector.DetectByPointSreen<Anchor>(data.MousePosition);
                if (anchor != null && _designer.MaintainedRoad != null)
                {
                    controlTransferred = true;

                    Debug.Log(1);

                    await _movementAnchorSubsystem.TransferControl(anchor, _designer);

                    controlTransferred = false;
                }
                else
                {
                    RaycastHit hit;
                    if (_detector.DetectByPointSreen(data.MousePosition, layerPlatform, out hit))
                    {
                        Debug.Log(2);

                        var position = hit.point;
                        position.y += 0.1f;
                        if (_designer.MaintainedRoad == null)
                            _designer.CreateRoad(settings, position);
                        else
                            _designer.AddAnchor(position);
                    }
                }
            }
            else if (data.Type == MouseInputType.RightMouseButtonDown)
            {
                controlTransferred = true;

                await _flightCameraSubsystem.TransferControl();

                controlTransferred = false;
            }
        }

        private bool UpdateControl(KeyboardInputType type)
            => type == KeyboardInputType.Esc ? true : false;
    }
}
