﻿using System;
using UnityEngine;
using Cysharp.Threading.Tasks;
using Zenject;
using Core.CameraFlightBehaviour;
using Utility;

namespace Architecture.Subsystems.FlightCamera
{
    using InputBehaviour;

    public class FlightCameraSubsystem : ISubsystemAsync
    {
        [Inject]
        CameraController _controller;

        [Inject]
        IBackgroundSubsystem<MouseData> _mouseInputSubsystem;

        public async UniTask TransferControl()
        {
            CursorUtility.MousePosition position;
            CursorUtility.GetCursorPos(out position);

            Cursor.lockState = CursorLockMode.Locked;
            _controller.EnableFlightMode();

            bool isBothMouseButtonsUp = false;
            Action<MouseData> onUpdateInput = data => { if (data.Type == MouseInputType.RightMouseButtonUp) isBothMouseButtonsUp = true; };

            _mouseInputSubsystem.OnBackgroundEvent += onUpdateInput;

            while (!isBothMouseButtonsUp) await UniTask.Yield();

            _controller.DisableFlightMode();

            Cursor.lockState = CursorLockMode.None;
            CursorUtility.SetCursorPos(position.x, position.y);

            _mouseInputSubsystem.OnBackgroundEvent -= onUpdateInput;
        }
    }
}
