using System;
using System.Threading;
using UnityEngine;
using Cysharp.Threading.Tasks;

namespace Architecture.Subsystems.InputBehaviour
{
    public class KeyboardInputSubsystem : IBackgroundSubsystem<KeyboardInputType>
    {
        private CancellationTokenSource _source;

        public event Action<KeyboardInputType> OnBackgroundEvent = delegate { };

        public void Start()
        {
            if (_source != null) throw new Exception();

            _source = new CancellationTokenSource();
            TrackInput(_source.Token);
        }


        public void Dispose()
        {
            _source?.Cancel();
            _source?.Dispose();
            _source = null;
        }

        private async void TrackInput(CancellationToken cancel)
        {
            while (!cancel.IsCancellationRequested)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                    OnBackgroundEvent(KeyboardInputType.Enter);

                if (Input.GetKeyDown(KeyCode.Escape))
                    OnBackgroundEvent(KeyboardInputType.Esc);

                if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.Z))
                {
                    Debug.Log("CtrlZ");
                    OnBackgroundEvent(KeyboardInputType.LeftShiftZ);
                }

                await UniTask.Yield();
            }
        }
    }

    public enum KeyboardInputType
    {
        Enter,
        Esc,
        LeftShiftZ,
    }
}