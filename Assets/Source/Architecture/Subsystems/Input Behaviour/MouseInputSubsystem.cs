﻿using System;
using System.Threading;
using UnityEngine;
using Cysharp.Threading.Tasks;

namespace Architecture.Subsystems.InputBehaviour
{
    public class MouseInputSubsystem : IBackgroundSubsystem<MouseData>
    {
        private CancellationTokenSource _source;

        public event Action<MouseData> OnBackgroundEvent = delegate { };

        public void Start()
        {
            if (_source != null) throw new Exception();

            _source = new CancellationTokenSource();
            TrackInput(_source.Token);
        }


        public void Dispose()
        {
            _source?.Cancel();
            _source?.Dispose();
            _source = null;
        }

        private async void TrackInput(CancellationToken cancel)
        {
            while (!cancel.IsCancellationRequested)
            {
                if (Input.GetMouseButtonDown(0)) 
                    OnBackgroundEvent(new MouseData(MouseInputType.LeftMouseButtonDown, Input.mousePosition));
  
                if (Input.GetMouseButtonDown(1))
                    OnBackgroundEvent(new MouseData(MouseInputType.RightMouseButtonDown, Input.mousePosition));

                if (Input.GetMouseButtonUp(1)) 
                    OnBackgroundEvent(new MouseData(MouseInputType.RightMouseButtonUp, Input.mousePosition));

                await UniTask.Yield();
            }
        }
    }

    public struct MouseData
    {
        public readonly MouseInputType Type;
        public readonly Vector3 MousePosition;

        public MouseData(MouseInputType inputType, Vector3 mousePosition)
        {
            Type = inputType;
            MousePosition = mousePosition;
        }
    }

    public enum MouseInputType
    {
        LeftMouseButtonDown,
        RightMouseButtonDown,
        RightMouseButtonUp,
    }
}