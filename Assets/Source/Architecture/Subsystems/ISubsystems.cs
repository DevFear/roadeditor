using System;
using Cysharp.Threading.Tasks;

/// <summary>
/// �������� ����� ������ ����������� �� ��������� ����������, 
/// ������� ����� ����������������� ���� � ������, �������� �������� ����� ���������� (�������������).
/// </summary>
namespace Architecture.Subsystems
{
    /// <summary>
    /// ������������ ��������� ���������� ��� ���������� ��������.
    /// </summary>
    public interface ISubsystem
    {
        /// <summary>
        /// ����� ����������, ����� ���������� ����� ��������� ���-���� ������� � �������� ��� ����������.
        /// </summary>
        public void TransferControl();
    }

    /// <summary>
    /// ���������� ������������� ���������� � ��������� ������.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISubsystem<T>
    {
        /// <summary>
        /// ����� ����������, ����� ���������� ����� ��������� ���-���� ������� � �������� ��� ����������.
        /// </summary>
        /// <param name="data"></param>
        public void TransferControl(T data);
    }

    /// <summary>
    /// ���������� ������������� ���������� � ��������� ������.
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    public interface ISubsystem<T1, T2>
    {
        /// <summary>
        /// ����� ����������, ����� ���������� ����� ��������� ���-���� ������� � �������� ��� ����������.
        /// </summary>
        /// <param name="data"></param>
        public void TransferControl(T1 data1, T2 data2);
    }

    /// <summary>
    /// ����������� ����������, ���������� � ��������� ��������� ������ ����������.
    /// </summary>
    public interface ISubsystemAsync
    {
        /// <summary>
        /// ����� ����������, ����� ���������� ����� ��������� ���-���� ������� � �������� ��� ����������
        /// � ����������� ��������� ���������� ���� ����������.
        /// </summary>
        /// <returns></returns>
        public UniTask TransferControl();
    }

    public interface ISubsystemAsync<T>
    {
        public UniTask TransferControl(T data);
    }

    public interface ISubsystemAsync<T1, T2>
    {
        public UniTask TransferControl(T1 data1, T2 data2);
    }

    public interface IResultingSubsystemAsync<T>
    {
        UniTask<T> TransferControl();
    }

    public interface IBackgroundSubsystem
    {
        void Start();
        void Dispose();
    }
    
    public interface IBackgroundSubsystem<T> : IBackgroundSubsystem
    {
        public event Action<T> OnBackgroundEvent;
    }
}