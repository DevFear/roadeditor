﻿using System;
using UnityEngine;
using Zenject;
using Cysharp.Threading.Tasks;

namespace Architecture.SystemsManagement
{
    using Subsystems;
    using Subsystems.InputBehaviour;

    public class SubsystemsDispatcher : IInitializable, IDisposable
    {
        IBackgroundSubsystem<MouseData> _mouseInputSubsystem;
        IBackgroundSubsystem<KeyboardInputType> _keyboardInputSubsystem;

        ISubsystemAsync _flightCameraSubsystem;
        ISubsystemAsync _roadBuildingSubsystem;

        bool controlTransferred; //заглушка для стейтов


        public SubsystemsDispatcher(IBackgroundSubsystem<MouseData> mouseInputSubsystem, IBackgroundSubsystem<KeyboardInputType> keyboardInputSubsystem,
            [Inject(Id = "Flight")] ISubsystemAsync flightCameraSubsystem, [Inject(Id = "Road")] ISubsystemAsync roadBuildingSubsystem)
        {
            _mouseInputSubsystem =  mouseInputSubsystem;
            _keyboardInputSubsystem = keyboardInputSubsystem;

            _flightCameraSubsystem = flightCameraSubsystem;
            _roadBuildingSubsystem = roadBuildingSubsystem;

            controlTransferred = false;
        }

        public void Initialize()
        {
            _mouseInputSubsystem.Start();
            _keyboardInputSubsystem.Start();

            _mouseInputSubsystem.OnBackgroundEvent += HandleMouseInput;
            _keyboardInputSubsystem.OnBackgroundEvent += HandleKeyboardInput;
        }

        public void Dispose()
        {
            _mouseInputSubsystem.OnBackgroundEvent -= HandleMouseInput;
            _keyboardInputSubsystem.OnBackgroundEvent -= HandleKeyboardInput;

            _mouseInputSubsystem.Dispose();
            _keyboardInputSubsystem.Dispose();
        }

        private async void HandleMouseInput(MouseData data)
        {
            if (data.Type != MouseInputType.RightMouseButtonDown)
                return;

            if (controlTransferred) 
                return;
            controlTransferred = true;

            await _flightCameraSubsystem.TransferControl();

            controlTransferred = false;
        }

        private async void HandleKeyboardInput(KeyboardInputType type)
        {
            if (type != KeyboardInputType.Enter)
                return;

            if (controlTransferred) 
                return;

            controlTransferred = true;

            await _roadBuildingSubsystem.TransferControl();

            controlTransferred = false;
        }
    }
}
