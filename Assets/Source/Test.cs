using System;
using UnityEngine;

using Core.MeshGeneration;
using Core.RoadBuilding;
using Utility;

public class Test : MonoBehaviour
{
    [SerializeField] Road _prefab;

    private void Awake()
    {
        //var road = GameObject.Instantiate(_prefab, Vector3.zero, Quaternion.identity) as Road;
        //var mesh = CreateRoad();

        //road.GetComponent<MeshFilter>().mesh = mesh;
    }

    private Mesh CreateRoad()
    {
        var width = 10f; //������ ������
        var maxRadius = 12f; //������ �������� �����������
        var minRadius = maxRadius - width; //������ �������� �����������

        var A = new Vector3(200, 1, -500);
        var B = new Vector3(0, 1, -100);
        var C = new Vector3(-300, 1, 0);

        var AB = B - A;
        var BC = C - B;
        var directionWidthAB = Quaternion.AngleAxis(90, Vector3.up) * AB.normalized;
        var directionWidthBC = Quaternion.AngleAxis(90, Vector3.up) * BC.normalized;

        var ALeft = A - directionWidthAB * width * .5f;
        var ARight = A + directionWidthAB * width * .5f;

        var BALeft = B - directionWidthAB * width * .5f;
        var BARight = B + directionWidthAB * width * .5f;

        var BCLeft = B - directionWidthBC * width * .5f;
        var BCRight = B + directionWidthBC * width * .5f;

        var CLeft = C - directionWidthBC * width * .5f;
        var CRight = C + directionWidthBC * width * .5f;

        //CreateDebugCube(ALeft, nameof(ALeft));
        //CreateDebugCube(ARight, nameof(ARight));

        //CreateDebugCube(BALeft, nameof(BALeft));
        //CreateDebugCube(BARight, nameof(BARight));

        //CreateDebugCube(BCLeft, nameof(BCLeft));
        //CreateDebugCube(BCRight, nameof(BCRight));


        //CreateDebugCube(CLeft, nameof(CLeft));
        //CreateDebugCube(CRight, nameof(CRight));

        //Right Line
        var directionAB = BARight - ARight;
        var directionCB = BCRight - CRight;

        Vector2 point;
        if (FindPointIntersection(new Vector2(ARight.x, ARight.z), new Vector2(BARight.x, BARight.z),
           new Vector2(CRight.x, CRight.z), new Vector2(BCRight.x, BCRight.z), out point))
        {
            //CreateDebugCube(new Vector3(point.x, 1, point.y), "Intersection Point 1");
        }

        var angle1 = Vector3.Angle(directionAB, directionCB);
        angle1 = 180f - angle1;
        Debug.Log(angle1);

        var rad1 = angle1 * Mathf.Deg2Rad;
        var T1 = maxRadius * Mathf.Tan(rad1 / 2);

        Debug.Log(T1);

        var pointIntersection = new Vector3(point.x, 1, point.y);

        var pointABRight = pointIntersection + -directionAB.normalized * T1;
        //CreateDebugCube(pointABRight, "New Point AB");

        var pointCBRight = pointIntersection + -directionCB.normalized * T1;
        //CreateDebugCube(pointCBRight, "New Point CB");


        var builder = new MeshBuilder("Road");

        //Left Line
        directionAB = BALeft - ALeft;
        directionCB = BCLeft - CLeft;

        if (FindPointIntersection(new Vector2(ALeft.x, ALeft.z), new Vector2(BALeft.x, BALeft.z),
         new Vector2(CLeft.x, CLeft.z), new Vector2(BCLeft.x, BCLeft.z), out point))
        {
            //CreateDebugCube(new Vector3(point.x, 1, point.y), "Intersection Point 2");
        }

        var angle2 = Vector3.Angle(directionAB, directionCB);
        angle2 = 180f - angle2;
        Debug.Log(angle2);

        var rad2 = angle2 * Mathf.Deg2Rad;
        var T2 = minRadius * Mathf.Tan(rad2 / 2);

        Debug.Log(rad1 + " --- " + rad2);

        pointIntersection = new Vector3(point.x, 1, point.y);

        var pointABLeft = pointIntersection + -directionAB.normalized * T2;
        //CreateDebugCube(pointABLeft, "New Point AB Left");

        var pointCBLeft = pointIntersection + -directionCB.normalized * T2;
        //CreateDebugCube(pointCBLeft, "New Point CB Left");
        var center = pointABRight - directionWidthAB * maxRadius;

        //builder.BuildQuad(ALeft, pointABLeft, pointABRight, ARight, Vector3.up);
        //builder.BuildRindSector(center, 25, maxRadius, minRadius, pointABRight, rad1);
        //builder.BuildQuad(CRight, pointCBRight, pointCBLeft, CLeft, Vector3.up);


        ////Circle
        
        //CreateDebugCube(center, "Center Circle");

        //center = pointCB - directionWidthBC * maxRadius;
        //CreateDebugCube(center, "Center Circle");

        //builder.BuildLapSector(center, 25, minRadius, rad2, pointABLeft);

        

        return builder.Create();
    }

    private bool FindPointIntersection(Vector2 Point1, Vector2 Point2, Vector2 Point3, Vector2 Point4, out Vector2 point)
    {
        point = Vector2.zero;

        var a1 = Point2.y - Point1.y;
        var b1 = Point1.x - Point2.x;
        var c1 = a1 * Point1.x + b1 * Point1.y;

        var a2 = Point4.y - Point3.y;
        var b2 = Point3.x - Point4.x;
        var c2 = a2 * Point3.x + b2 * Point3.y;

        var delta = a1 * b2 - a2 * b1;
        if (delta == 0)
            return false;

        var x = (b2 * c1 - b1 * c2) / delta;
        var y = (a1 * c2 - a2 * c1) / delta;

        point = new Vector2(x, y); 
        return true;
    }

    //private bool CalculatePointIntersection(Vector3 A1, Vector3 A2, Vector3 B1, Vector3 B2, out Vector3 point)
    //{
    //    return false;
    //}

    //�������� ����������� �����
    private void CreateDebugCube(Vector3 position, string name)
    {
        var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = position;
        cube.name = name;
    }
}
