using System.Runtime.InteropServices;
using UnityEngine;

namespace Utility
{
    static class CursorUtility
    {
        [DllImport("user32.dll")]
        public static extern bool SetCursorPos(int x, int y);

        [DllImport("user32.dll")]
        public static extern bool GetCursorPos(out MousePosition position);

        [StructLayout(LayoutKind.Sequential)]
        public struct MousePosition
        {
            public int x;
            public int y;
        }
    }
}