﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.RoadBuilding.Designing
{ 
    public class RoadDesigner
    {
        Road.Factory _roadFactory;
        Anchor.Factory _anchorFactory;

        Road _maintainedRoad;
        State _state;

        public RoadDesigner(Road.Factory roadFactory, Anchor.Factory anchorFactory)
        {
            _roadFactory = roadFactory;
            _anchorFactory = anchorFactory;
        }

        public Road MaintainedRoad => _maintainedRoad;

        public void CreateRoad(RoadSettings settings, Vector3 positionAnchor)
        {
            if (_maintainedRoad != null) throw new Exception();

            var anchor = _anchorFactory.Create(positionAnchor);
            _maintainedRoad = _roadFactory.Create(anchor, settings);

            _state = new OneAnchorState(settings);
            var data = _state.RecalculateMesh(new List<Anchor>() { anchor });

            _state = data.Item1;
            _maintainedRoad.UpdateMesh(data.Item2);
        }

        public void EditRoad(Road road)
        {
            if (_maintainedRoad != null) throw new Exception();

            _maintainedRoad = road;
            _maintainedRoad.ActivateAnchors();

            switch (_maintainedRoad.Anchors.Count)
            {
                case 1:
                    _state = new OneAnchorState(_maintainedRoad.Settings);
                    break;
                case 2:
                    _state = new TwoAnchorState(_maintainedRoad.Settings);
                    break;
                case 3:
                    _state = new ThreeAnchorState(_maintainedRoad.Settings);
                    break;
                case 4:
                    _state = new MultiAnchorState(_maintainedRoad.Settings);
                    break;
            }
        }

        public void MoveAnchor(Anchor anchor, Vector3 position)
        {
            if (_maintainedRoad == null)
                throw new Exception();

            anchor.Position = position;

            var data = _state.RecalculateMesh(_maintainedRoad.Anchors);

            _state = data.Item1;
            _maintainedRoad.UpdateMesh(data.Item2);
        }


        public void CloseRoad()
        {
            _maintainedRoad.DiactivateAnchors();
            _maintainedRoad = null;
            _state = null;
        }

        public void AddAnchor(Vector3 positionAnchor)
        {
            var anchor = _anchorFactory.Create(positionAnchor);

            _maintainedRoad.AddAnchor(anchor);

            var data = _state.RecalculateMesh(_maintainedRoad.Anchors);
            _state = data.Item1;

            _maintainedRoad.UpdateMesh(data.Item2);
        }
    }
}
