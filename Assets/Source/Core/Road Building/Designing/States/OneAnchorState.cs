﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.RoadBuilding.Designing
{
    public class OneAnchorState : State
    {
        Mesh _emptyMesh;

        public OneAnchorState(RoadSettings settings)
            : base(settings)
        {
            _emptyMesh = new Mesh()
            {
                name = NameMesh
            };
        }


        public override (State, Mesh) RecalculateMesh(IReadOnlyList<Anchor> anchors)
        {
            if (anchors.Count == 1)
                return (this, _emptyMesh);
            else if (anchors.Count == 2)
            {
                var state = new TwoAnchorState(settings);
                var data = state.RecalculateMesh(anchors);

                return (state, data.Item2);
            }
            else 
                throw new System.Exception();
        }
    }
}
