﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Core.MeshGeneration;
using Utility;

namespace Core.RoadBuilding.Designing
{
    public class MultiAnchorState : State
    {
        public MultiAnchorState(RoadSettings settings)
            : base(settings) { }

        public override (State, Mesh) RecalculateMesh(IReadOnlyList<Anchor> anchors)
        {
            var builder = new MeshBuilder(NameMesh);

            (Segment segment, bool isCalculated) lastCalculate;
            lastCalculate.isCalculated = false;
            lastCalculate.segment = default;

            for (int i = 0; i < anchors.Count - 2; i++)
            {
                var segment = DesignTools.CalculateSegment(anchors[i].Position, anchors[i + 1].Position, anchors[i + 2].Position, settings);

                if (!lastCalculate.isCalculated)
                {
                    lastCalculate.segment = segment;
                    lastCalculate.isCalculated = true;

                    builder.BuildQuad(
                       segment.OffsetStartAnchor.Left,
                       segment.LeftBreakPoints.Start,
                       segment.RightBreakPoints.Start,
                       segment.OffsetStartAnchor.Right,
                       Vector3.up);

                    builder.BuildRindSector(
                        segment.Center,
                        segment.localXCircle,
                        Vector3.up,
                        segment.Segmentation,
                        settings.MaxRadius,
                        settings.MinRadius,
                        segment.Rad);
                }
                else
                {
                    var lastSegment = lastCalculate.segment;

                    builder.BuildQuad(
                        lastSegment.LeftBreakPoints.End,
                        segment.LeftBreakPoints.Start,
                        segment.RightBreakPoints.Start,
                        lastSegment.RightBreakPoints.End,
                        Vector3.up);

                    builder.BuildRindSector(
                     segment.Center,
                     segment.localXCircle,
                     Vector3.up,
                     segment.Segmentation,
                     settings.MaxRadius,
                     settings.MinRadius,
                     segment.Rad);

                    if (i + 2 == anchors.Count - 1)
                        builder.BuildQuad(
                            segment.LeftBreakPoints.End,
                            segment.OffsetEndAnchor.Left,
                            segment.OffsetEndAnchor.Right,
                            segment.RightBreakPoints.End,
                            Vector3.up);

                    lastCalculate.segment = segment;
                }
            }

            return (this, builder.Create());
        }
    }
}
