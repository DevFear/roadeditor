using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Core.MeshGeneration;

namespace Core.RoadBuilding.Designing
{
    public abstract class State
    {
        private static protected string NameMesh = "Road";

        protected readonly RoadSettings settings;

        protected State(RoadSettings settings) => this.settings = settings;
        public abstract (State, Mesh) RecalculateMesh(IReadOnlyList<Anchor> anchors);
    }
}