using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Core.RoadBuilding
{
    public class Road : MonoBehaviour
    {
        MeshFilter _filter;
        List<Anchor> _anchors;

        private void Init(Anchor openerAnchor, RoadSettings settings)
        {
            transform.position = Vector3.zero; //временно

            Settings = settings;

            _filter = gameObject.GetComponent<MeshFilter>();
            _anchors = new List<Anchor>();

            _anchors.Add(openerAnchor);
            openerAnchor.SetRoad(transform);    
        }

        public IReadOnlyList<Anchor> Anchors => _anchors;
        public RoadSettings Settings { get; private set; }

        public void AddAnchor(Anchor anchor)
        {
            var last = _anchors[_anchors.Count - 1];
            last.BindAnchor(anchor);

            _anchors.Add(anchor);
            anchor.SetRoad(transform);
        }

        public void UpdateMesh(Mesh mesh) => _filter.mesh = mesh;

        public void ActivateAnchors()
        {
            foreach (var anchor in _anchors)
                anchor.SetActive(true);
        }

        public void DiactivateAnchors()
        {
            foreach (var anchor in _anchors)
                anchor.SetActive(false);
        }


        public class Factory : IFactory<Anchor, RoadSettings, Road>
        {
            Road _prefab;
            Transform _group;

            public Factory(Road prefab, Anchor.Factory factory)
            {
                _prefab = prefab;
                _group = new GameObject("Roads").transform;
            }

            public Road Create(Anchor openerAnchor, RoadSettings settings)
            {
                var road = GameObject.Instantiate(_prefab, _group).GetComponent<Road>();
                road.Init(openerAnchor, settings);

                return road;
            }
        }
    }
}