using System;
using System.Threading;
using UnityEngine;
using Cysharp.Threading.Tasks;

namespace Core.CameraFlightBehaviour
{
    public class CameraController
    {
        [SerializeField] float speed = 100f;
        [SerializeField] float sensitivity = 5f;

        CancellationTokenSource _sourceFlightMode;

        Camera _camera;
        Transform _transform;

        public CameraController(Camera camera)
        {
            _camera = camera;
            _transform = _camera.transform;
        }

        public Camera Camera => _camera;

        public async void EnableFlightMode()
        {
            if (_sourceFlightMode != null) throw new Exception();
            _sourceFlightMode = new CancellationTokenSource();

            var token = _sourceFlightMode.Token;

            while (!token.IsCancellationRequested)
            {
                await UniTask.Yield();
                if (this == null) return;
                UpdateFlight();
            }
        }

        public void DisableFlightMode()
        {
            _sourceFlightMode?.Cancel();
            _sourceFlightMode?.Dispose();
            _sourceFlightMode = null;
        }

        private void UpdateFlight()
        {
            var direction = GetDirection(_transform.forward, _transform.right);
            _transform.position += direction * speed * Time.deltaTime;

            _transform.eulerAngles += GetRotation() * sensitivity;
        }

        private Vector3 GetDirection(Vector3 forward, Vector3 right)
            => forward * Input.GetAxis("Vertical") + right * Input.GetAxis("Horizontal");
        private Vector3 GetRotation()
            => new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0);

    }
}
