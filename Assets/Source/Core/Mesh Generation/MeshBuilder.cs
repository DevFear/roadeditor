using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.MeshGeneration
{
    public class MeshBuilder
    {
        private List<Vector3> _vertices;
        private List<Vector3> _normals;
        private List<Vector2> _uvs;
        private List<int> _indexes;

        public readonly string Name;

        public MeshBuilder(string name)
        {
            Name = name;

            _vertices = new List<Vector3>();
            _normals = new List<Vector3>();
            _uvs = new List<Vector2>();
            _indexes = new List<int>();
        }

        public MeshBuilder(Mesh mesh)
        {
            Name = mesh.name;

            _vertices = new List<Vector3>(mesh.vertices);
            _normals = new List<Vector3>(mesh.normals);
            _uvs = new List<Vector2>();
            _indexes = new List<int>();
        }

        public IReadOnlyCollection<Vector3> Vertices => _vertices;
        public IReadOnlyCollection<Vector3> Normals => _normals;
        public IReadOnlyCollection<Vector2> UVs => _uvs;
        public IReadOnlyCollection<int> Indexes => _indexes;

        public Mesh Create()
        {
            var mesh = new Mesh()
            {
                vertices = _vertices.ToArray(),
                triangles = _indexes.ToArray(),
                normals = _normals.ToArray(),
                uv = _uvs.ToArray(),
                name = Name,
            };

            mesh.RecalculateBounds();
            mesh.RecalculateNormals();

            return mesh;
        }

        public void AddVertices(Vector3 vertice, Vector3 normal, Vector2 uv)
        {
            _vertices.Add(vertice);
            _normals.Add(normal);
            _uvs.Add(uv);
        }

        public void AddTriangle(int index0, int index1, int index2)
        {
            _indexes.Add(index0);
            _indexes.Add(index1);
            _indexes.Add(index2);
        }

        public void BuildQuad(Vector3 zero, Vector3 first, Vector3 second, Vector3 third, Vector3 normal)
        {
            AddVertices(zero, normal, Vector3.zero);
            AddVertices(first, normal, Vector2.up);
            AddVertices(second, normal, Vector2.one);
            AddVertices(third, normal, Vector2.right);

            var baseIndex = _vertices.Count - 4;

            AddTriangle(baseIndex, baseIndex + 1, baseIndex + 2);
            AddTriangle(baseIndex, baseIndex + 2, baseIndex + 3);
        }

        public void BuildRindSector(Vector3 center, Vector3 localX, Vector3 normal, int segmentCount, float maxRadius, float minRadius, float radRing, 
            bool isDirectOrderRendering = true)
        {
            var angleInc = radRing / segmentCount;
            var localZ = Quaternion.AngleAxis(90f, normal) * localX;

            for (int i = 0; i <= segmentCount; i++)
            {
                var angle = angleInc * i;
                var vertex = localX * Mathf.Cos(angle) + localZ * Mathf.Sin(angle);

                AddVertices(center + vertex * minRadius, normal, new Vector2(vertex.x + 1f, vertex.z + 1f) * .5f);
                AddVertices(center + vertex * maxRadius, normal, new Vector2(vertex.x + 1f, vertex.z + 1f) * .5f);

                if (i > 0)
                {
                    var baseIndex = _vertices.Count - 4;

                    if (isDirectOrderRendering)
                    {
                        AddTriangle(baseIndex, baseIndex + 1, baseIndex + 2);
                        AddTriangle(baseIndex + 1, baseIndex + 3, baseIndex + 2);
                    }
                    else
                    {
                        AddTriangle(baseIndex + 2, baseIndex + 3, baseIndex + 1);
                        AddTriangle(baseIndex + 1, baseIndex, baseIndex + 2);
                    }
                    
                }
            }
        }
    }
}