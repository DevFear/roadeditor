Shader "HPEW/Line render effect"
{
    Properties
    {
        [HDR]_Color("Color", Color) = (1,1,1,1)
        _MainTex ("Particles texture" , 2D) = "white" {}
        _Gradient("Gradient", 2D) = "black" {}
        _TransparentAmount("Transparent amount", Float) = 0

       // [HDR] _RimColor("Rim Color", Color) = (1,1,1,1)
            _Speed("Speed",Float) = 0

            _Distantion("Clip value", Float) = 1

    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
        LOD 100

        Blend SrcAlpha OneMinusSrcAlpha
                ZWrite ON

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #pragma target 2.0

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uv_Gradient : TEXCOORD1;
                float4 vertex : SV_POSITION;
                float4 worldPos : TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _Gradient;
            float4 _Gradient_ST;
            float _TransparentAmount;
            fixed4 _Color;
            float _Distantion;

            //float4 _RimColor;
            float _Speed;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv_Gradient = TRANSFORM_TEX(v.uv, _Gradient);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);


                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, float2(i.uv.x + (_Time.y * _Speed), i.uv.y)) * _Color;
                
                fixed4 grad = tex2D(_Gradient, i.uv_Gradient);
                
                col.a = max(0.15f, ((1 - grad.r) + _TransparentAmount) * ((1 - grad.r) + _TransparentAmount));
             
                if (i.uv_Gradient.y <= 0.1f)
                {
                    col.a = lerp(col.a + 1, col.a, i.uv_Gradient.y * 10) + col.a;
                    //col.rgb = _RimColor * abs(i.uv_Gradient.x - 0.5f) + 0.5f;
                    col.rgb = lerp(_Color * abs(i.uv_Gradient.x - 0.5f) + 0.5f, col.rgb, i.uv_Gradient.y * 10);
                }
                else if (i.uv_Gradient.y >= 0.9f)
                {
                    col.a = lerp(col.a + 1, col.a, (1 - i.uv_Gradient.y) * 10) + col.a;
                    //col.rgba *= lerp(col, _RimColor, i.uv_Gradient.y * 10);
                    col.rgb = lerp(_Color * abs(i.uv_Gradient.x - 0.5f) + 0.5f, col.rgb, (1 - i.uv_Gradient.y) * 10);
                }

                float dist = distance(i.worldPos, _WorldSpaceCameraPos);
                col.a *= saturate(dist - _Distantion);
                

                return col;
            }
            ENDCG
        }
    }
}
